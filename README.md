[![MIT License][li]][ll]

# Cookie Solution
#### Author: 
- Luca Pulira
- luca.pulira@lp741.com

## What is Cookie Solution?

Cookie Solution is a JavaScript widget we build to ensure our clients’ sites comply with all legal requirements regarding cookies. Basically, it’s one of the “do you want to allow cookies?” banners you saw a million times on the web. One of the critical legal requirements is that a user should be informed about the purposes of each tracking cookie, in general, and on each tracker in particular.

## Demo

You can find a working demo [here](https://cookie-solution.vercel.app/)

## Requirements

This solution requires [**Node**](https://nodejs.org/) and [**pnpm**](https://pnpm.io/) (package manager) installed.

If you prefer to use other package managers change package.json script section replacing pnpm commands with yarn or npm

_package.json_
```json
  "scripts": {
    "dev": "vite --port 3000",
    "dev:debug": "vite --port 3000 --debug",
    "build": "pnpm build:lib & pnpm build:dashboard & pnpm build:demo",
    "build:lib": "vite -c vite-lib.config.js build",
    "build:dashboard": "vite -c vite-dashboard.config.js build",
    "build:demo": "vite -c vite-demo.config.js build",
    "preview": "vite -c vite-demo.config.js preview"
  }
```
It is based on [**Vite**](https://vitejs.dev/) build tool

## Build libraries and Demo

```bash
pnpm i

pnpm build
```
It creates _dist_ folder with the following structure

<pre>
dist
|-- banner
|  |-- cookie-solution.js
|  |-- cookie-solution.umd.cjs
|-- dashboard
|  |-- cookie-solution-dashboard.js
|  |-- cookie-solution-dashboard.umd.cjs
|-- favicon.svg
|-- index.html
|-- main.js
|-- main.umd.cjs
|-- style.css
</pre>

## Quick Start

Just copy built library in **dist/cookie-solution** in your html page or application static folder and add script and css file at the bottom of head or body.

You can find an example in _dist/index.html_

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    ...
    <link
      href="cookie-solution/cookie-solution.css"
      rel="stylesheet"
      type="text/css"
    />
    ...
  </head>
  <body>
    ...
    <script src="cookie-solution/cookie-solution.js" type="module"></script>
    ...
```

It shows a popup until you not push reject or accept then it will be replaced with a floating Edit Preferences button

Reject and Accept actions create and update the following cookies:
```
cookie-solution-accepted
cookie-solution-p1
cookie-solution-p2
cookie-solution-p3
cookie-solution-p4
```

every cookie has a boolean value true or false depends on checkboxes checked
cookie-solution-accepted is true if Accept clicked or false if Reject clicked

if cookie-solution-accepted is defined popup will not be showed automatically anymore

## Advanced Demo Page

Solution based on modules and static libraries
```bash
pnpm build

pnpm preview
```

Fast Vite solution (it creates dist-demo folder)

```
pnpm demo
```

Open http://127.0.0.1:3000/ in your browser

## Development

- Library source files are located in src folder under cookie_solution
- src/dashboard contains files relative to demo page logic
- Demo page style is defined in assets/main.js and assets/style.css 

Just type
```bash
pnpm i 

pnpm dev
```

Open http://127.0.0.1:3000/ in your browser

---

## Test

```
pnpm test:lib
```

[li]: https://img.shields.io/badge/license-MIT-brightgreen.svg
[ll]: LICENSE
