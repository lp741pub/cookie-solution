import { defineConfig } from "vite";
import path from "path";
export default defineConfig({
  publicDir: false,
  build: {
    rollupOptions: {
      output: {
        assetFileNames: (chunkInfo) => {
          if (chunkInfo.name === "style.css") return "cookie-solution.css";
        },
      },
    },
    outDir: "dist/cookie_solution",
    lib: {
      entry: path.resolve(__dirname, "src/cookie_solution/cookie-solution.js"),
      fileName: "cookie-solution",
      name: "CookieSolutionLib",
    },
  },
  test: {
    // ...
  },
});
