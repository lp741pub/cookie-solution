import { defineConfig } from "vite";
import path from "path";
export default defineConfig({
  build: {
    outDir: "dist",
    lib: {
      entry: path.resolve(__dirname, "assets/main.js"),
      fileName: "main",
      name: "CookieSolutionDemo",
    },
  },
});
