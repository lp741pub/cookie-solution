import { defineConfig } from "vite";
import path from "path";
export default defineConfig({
  publicDir: false,
  build: {
    outDir: "dist/dashboard",
    rollupOptions: {
      output: {
        assetFileNames: (chunkInfo) => {
          if (chunkInfo.name === "style.css")
            return "cookie-solution-dashboard.css";
        },
      },
    },
    lib: {
      entry: path.resolve(
        __dirname,
        "src/dashboard/cookie-solution-dashboard.js"
      ),
      fileName: "cookie-solution-dashboard",
      name: "CookieSolutionDashboard",
    },
  },
});
