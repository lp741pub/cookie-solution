import "./cookie-solution-dashboard.css";
import { Cookie } from "../cookie.js";

(function () {
  const { getCookie } = Cookie();
  const purposes = [];
  const accepted = getCookie("cookie-solution-accepted");
  for (let i = 0; i < 4; i++) {
    purposes[i] = getCookie(`cookie-solution-p${i + 1}`);
  }
  if (typeof accepted !== "undefined") {
    if (!purposes[0] && !purposes[1] && !purposes[2] && !purposes[3]) {
      document.getElementById("csNoPurpose").classList.add("enabled");
    } else {
      for (let i = 0; i < purposes.length; i++) {
        if (purposes[i]) {
          document.getElementById(`csPurpose${i + 1}`).classList.add("enabled");
        }
      }
      if (purposes[0] && purposes[2]) {
        document.getElementById("csPurposes13").classList.add("enabled");
      }
    }
  }
})();
