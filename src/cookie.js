/**
 * Cookie helper function library
 *
 * @param {Object} windowDom - default browser window it could be mocked
 * @returns {Object}
 */
const Cookie = (windowDom) => {
  const windowObj = windowDom || window;
  const { document } = windowObj;

  return {
    /**
     * setCookie helper function to set cookie in browser
     * @param cname
     * @param cvalue
     * @param exdays
     */
    setCookie(cname, cvalue, exdays = undefined) {
      const d = new Date();
      d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
      let expires = "expires=" + d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    /**
     * getCookie helper function to get cookie value, if cname doesn't exists return undefined
     * @param cname
     * @returns {number|boolean|string|undefined}
     */
    getCookie(cname) {
      let name = cname + "=";
      let decodedCookie = decodeURIComponent(document.cookie);
      let ca = decodedCookie.split(";");
      for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === " ") {
          c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
          const strValue = c.substring(name.length, c.length);

          if (
            strValue.toLocaleLowerCase() === "false" ||
            strValue.toLocaleLowerCase() === "true"
          ) {
            return strValue.toLocaleLowerCase() === "true";
          }

          if (strValue.matchAll(/[0-9.,]*/)) {
            return Number(strValue);
          }

          return c.substring(name.length, c.length);
        }
      }
      return undefined;
    },
  };
};

export { Cookie };
