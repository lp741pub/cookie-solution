import "./cookie-solution.css";
import { CookieSolution } from "./cookie-solution-core.js";

(function () {
  const cs = new CookieSolution();
  cs.init();
})();
