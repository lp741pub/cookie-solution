import { Cookie } from "../cookie";

class CookieSolution {
  /**
   * CookieSolution
   *
   * @param {Object} windowDom -- dafeult browser window object could be mocked
   */
  constructor(windowDom = null) {
    const windowObj = windowDom || window;
    this.cookie = Cookie(windowObj);
    this.window = windowObj;

    this.cookieSolutionAcceptSelectedCookies =
      this.cookieSolutionAcceptSelectedCookies.bind(this);
    this.cookieSolutionResetAllCookies =
      this.cookieSolutionResetAllCookies.bind(this);
    this.openEditPreferences = this.openEditPreferences.bind(this);
  }

  /**
   * isAccepted - user accepted some purpose or nothing but not rejected
   *
   * @returns {number|boolean|string|undefined}
   */
  isAccepted() {
    const { getCookie } = this.cookie;
    return getCookie("cookie-solution-accepted");
  }

  /**
   * getAllPurposeCookies - return all purpose cookies
   *
   * @returns {number|boolean|string|undefined[]}
   */
  getAllPurposeCookies() {
    const { getCookie } = this.cookie;
    const purposes = [];
    for (let i = 0; i < 4; i++) {
      purposes[i] = getCookie(`cookie-solution-p${i + 1}`);
    }
    return purposes;
  }

  reloadPage() {
    const { location } = this.window;
    location.reload();
  }

  /**
   * on click action cookieSolutionAcceptSelectedCookies
   * read choice from checkboxes ad set right cookies
   */
  cookieSolutionAcceptSelectedCookies() {
    const checkboxes = document.querySelectorAll(
      ".cookie-solution__purposes-list input[type=checkbox]:checked"
    );
    const newValues = [false, false, false, false];
    for (let i = 0; i < checkboxes.length; i++) {
      newValues[checkboxes[i].value - 1] = true;
    }

    const { setCookie } = this.cookie;
    setCookie("cookie-solution-accepted", true);
    for (let i = 0; i < 4; i++) {
      setCookie(`cookie-solution-p${i + 1}`, newValues[i]);
    }
  }

  /**
   * reject function reset all cookies to false
   */
  cookieSolutionResetAllCookies() {
    const { setCookie } = this.cookie;
    setCookie("cookie-solution-accepted", false);
    for (let i = 0; i < 4; i++) {
      setCookie(`cookie-solution-p${i + 1}`, false);
    }
  }

  /**
   * generateWidget - generate banner widget html
   *
   * @param {boolean[]} purposes
   * @returns {*}
   */
  generateWidget(purposes) {
    const cookieText =
      "Selected third parties, and we use cookies for technical purposes and, with your consent, for other purposes.";
    const acceptButtonLabel = "Accept";
    const rejectButtonLabel = "Reject";
    const purposeLabel = "Purpose";

    let purposeHtml = "";
    for (let i = 0; i < 4; i++) {
      purposeHtml = `${purposeHtml} <li><input type="checkbox" ${
        purposes[i] ? "checked" : ""
      } value="${
        i + 1
      }" name="cookieSolutionPurposeSelected"> ${purposeLabel} ${i + 1}</li>`;
    }
    const { document } = this.window;
    // add Cookie banner slided on the bottom of the page
    const widget = document.createElement("div");
    widget.classList.add("cookie-solution");
    widget.innerHTML = `
    <div class="cookie-solution__container">
      <div class="cookie-solution__text">${cookieText}</div>
      <ul class="cookie-solution__purposes-list">
        ${purposeHtml}
      </ul>
      <div class="cookie-solution__actions">
        <button class="cookie-solution__reject">${rejectButtonLabel}</button>
        <button class="cookie-solution__accept">${acceptButtonLabel}</button>
      </div>
    </div>
  `;

    const rejectButton = widget.querySelector(
      ".cookie-solution__actions .cookie-solution__reject"
    );
    rejectButton.addEventListener("click", () => {
      this.cookieSolutionResetAllCookies();
      this.reloadPage();
    });

    const acceptButton = widget.querySelector(
      ".cookie-solution__actions .cookie-solution__accept"
    );
    acceptButton.addEventListener("click", () => {
      this.cookieSolutionAcceptSelectedCookies();
      this.reloadPage();
    });

    return widget;
  }

  /**
   * createFloatingButton - generate floating button html
   *
   * @returns {HTMLDivElement}
   */
  createFloatingButton() {
    // add Floating edit preferences button
    const editButtonLabel = "Edit Preferences";
    const floatingButton = document.createElement("div");
    floatingButton.classList.add("cookie-solution-edit");
    floatingButton.innerHTML = `<button>${editButtonLabel}</button>`;

    const editPreferenceBtn = floatingButton.querySelector(
      ".cookie-solution-edit button"
    );
    editPreferenceBtn.addEventListener("click", this.openEditPreferences);

    return floatingButton;
  }

  openEditPreferences() {
    const { document } = this.window;
    const cookieSolutionBanenr = document.querySelector(".cookie-solution");
    if (cookieSolutionBanenr) {
      cookieSolutionBanenr.classList.add("show");
    }
  }

  /**
   * append banner to the bottom of BODY tag and show it after 1 sec
   * or shows edit preferences button if any cookie already set
   */
  init() {
    const { document } = this.window;

    const accepted = this.isAccepted();
    const purposes = this.getAllPurposeCookies();

    const widget = this.generateWidget(purposes);

    document.body.appendChild(widget);

    // if not accepted or rejected show cookie banner after 1 second
    if (typeof accepted === "undefined") {
      setTimeout(() => {
        this.openEditPreferences();
      }, 1000);
    } else {
      const floatingButton = this.createFloatingButton();
      document.body.appendChild(floatingButton);
    }
  }
}

export { CookieSolution };
