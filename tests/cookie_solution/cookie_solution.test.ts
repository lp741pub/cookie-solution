import { beforeEach, describe, expect, it } from "vitest";
import { createNewDom } from "../mock/cookie-mock";
import { CookieSolution } from "../../src/cookie_solution/cookie-solution-core";
import { Cookie } from "../../src/cookie";

describe("Cookie Solution library", () => {
  const cookieNames = [
    "cookie-solution-accepted",
    "cookie-solution-p1",
    "cookie-solution-p2",
    "cookie-solution-p3",
    "cookie-solution-p4",
  ];

  beforeEach(() => {});

  it("Integration test", () => {
    const domMock = createNewDom({ runScripts: "outside-only" });
    const cs = new CookieSolution(domMock.window);
    cs.init();

    const element = domMock.window.document.querySelector(".cookie-solution");

    expect(element).toBeDefined();
  });

  it("isAccepted", () => {
    const domMock = createNewDom({ runScripts: "outside-only" });
    const cs = new CookieSolution(domMock.window);
    const { setCookie } = Cookie(domMock.window);

    setCookie(cookieNames[0], true);
    setCookie(cookieNames[1], false);
    setCookie(cookieNames[2], false);
    setCookie(cookieNames[3], false);
    setCookie(cookieNames[4], false);

    const cookieAccepted = cs.isAccepted();

    expect(domMock.window.document.cookie).toBeDefined();
    expect(domMock.window.document.cookie.includes(cookieNames[0])).toBe(true);
  });
});
