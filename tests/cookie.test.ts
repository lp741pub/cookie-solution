import { assert, beforeEach, describe, expect, it, vi } from "vitest";
import {
  FULL_ACCEPTED_COOKIE,
  UNSET_COOKIE,
  FULL_REJECTED_COOKIE,
  PURPOSE_1_ACCEPTED_COOKIE,
  getStringSetCookieByName,
  createNewDom,
  PURPOSE_1_3_ACCEPTED_COOKIE,
} from "./mock/cookie-mock";
import { Cookie } from "../src/cookie";

describe("Cookie helpers library", () => {
  const cookieNames = [
    "cookie-solution-accepted",
    "cookie-solution-p1",
    "cookie-solution-p2",
    "cookie-solution-p3",
    "cookie-solution-p4",
  ];

  beforeEach(() => {});

  it("getCookie", () => {
    const domMock = createNewDom();

    const { getCookie } = Cookie(domMock.window);

    expect(
      getCookie,
      "getCookie function from Cookie library exists"
    ).toBeDefined();
  });

  it("getCookie(<every cookies>) FULL_ACCEPTED_COOKIE", () => {
    const domMock = createNewDom();

    const { getCookie } = Cookie(domMock.window);

    for (let i = 0; i < cookieNames.length; i++) {
      const cookieName = cookieNames[i];
      domMock.window.document.cookie = getStringSetCookieByName(
        cookieName,
        true
      );
    }

    expect(domMock.window.document.cookie).eq(FULL_ACCEPTED_COOKIE);

    for (let i = 0; i < cookieNames.length; i++) {
      const cookieName = cookieNames[i];
      const cookieValue = getCookie(cookieName);
      expect(cookieValue, `expect ${cookieName} exists`).toBeDefined();
      expect(cookieValue, `expect ${cookieName} exists`).toBeTruthy();
    }
  });

  it("getCookie(<every cookies>) UNSET_COOKIE", () => {
    const domMock = createNewDom();

    const { getCookie } = Cookie(domMock.window);

    expect(domMock.window.document.cookie).eq(UNSET_COOKIE);

    for (let i = 0; i < cookieNames.length; i++) {
      const cookieName = cookieNames[i];
      const cookieValue = getCookie(cookieName);
      expect(cookieValue, `expect ${cookieName} not defined`).toBeUndefined();
    }
  });

  it("getCookie(<every cookies>) FULL_REJECTED_COOKIE", () => {
    const domMock = createNewDom();

    const { getCookie } = Cookie(domMock.window);

    for (const cookieName of cookieNames) {
      domMock.window.document.cookie = getStringSetCookieByName(
        cookieName,
        false
      );
    }

    expect(domMock.window.document.cookie).eq(FULL_REJECTED_COOKIE);

    for (let i = 0; i < cookieNames.length; i++) {
      const cookieName = cookieNames[i];
      const cookieValue = getCookie(cookieName);
      expect(cookieValue, `expect ${cookieName} exists`).toBeDefined();
      expect(cookieValue, `expect ${cookieName} exists`).eq(false);
    }
  });

  it("setCookie", () => {
    const domMock = createNewDom();

    const { setCookie } = Cookie(domMock.window);

    expect(
      setCookie,
      "setCookie function from Cookie library exists"
    ).toBeDefined();
  });

  it("setCookie PURPOSE_1_ACCEPTED_COOKIE", () => {
    const domMock = createNewDom();

    const { setCookie } = Cookie(domMock.window);

    setCookie(cookieNames[0], true);
    setCookie(cookieNames[1], true);
    setCookie(cookieNames[2], false);
    setCookie(cookieNames[3], false);
    setCookie(cookieNames[4], false);

    expect(domMock.window.document.cookie).eq(PURPOSE_1_ACCEPTED_COOKIE);
  });

  it("setCookie PURPOSE_1_3_ACCEPTED_COOKIE", () => {
    const domMock = createNewDom();

    const { setCookie } = Cookie(domMock.window);

    setCookie(cookieNames[0], true);
    setCookie(cookieNames[1], true);
    setCookie(cookieNames[2], false);
    setCookie(cookieNames[3], true);
    setCookie(cookieNames[4], false);

    expect(domMock.window.document.cookie).eq(PURPOSE_1_3_ACCEPTED_COOKIE);
  });
});
