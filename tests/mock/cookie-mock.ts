import jsdom from "jsdom";

const { JSDOM } = jsdom;

export const UNSET_COOKIE = "";

export const FULL_ACCEPTED_COOKIE =
  "cookie-solution-accepted=true; cookie-solution-p1=true; cookie-solution-p2=true; cookie-solution-p3=true; cookie-solution-p4=true";

export const FULL_REJECTED_COOKIE =
  "cookie-solution-accepted=false; cookie-solution-p1=false; cookie-solution-p2=false; cookie-solution-p3=false; cookie-solution-p4=false";

export const PURPOSE_1_3_ACCEPTED_COOKIE =
  "cookie-solution-accepted=true; cookie-solution-p1=true; cookie-solution-p2=false; cookie-solution-p3=true; cookie-solution-p4=false";

export const PURPOSE_1_ACCEPTED_COOKIE =
  "cookie-solution-accepted=true; cookie-solution-p1=true; cookie-solution-p2=false; cookie-solution-p3=false; cookie-solution-p4=false";

export const getStringSetCookieByName = (cname, cvalue) => {
  const d = new Date();
  const exdays = undefined;
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = "expires=" + d.toUTCString();
  return cname + "=" + cvalue + ";" + expires + ";path=/";
};

export const createNewDom = (options = {}) => {
  return new JSDOM(
    `
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <title>Cookie Solution MOCK</title>
      </head>
      <body>
        <div class="container">
          <h1>Cookie Solution</h1>
          <div id="cookieSolutionConfig" class="cookie-solution__config">
            <div id="csNoPurpose">No purpose</div>
            <div id="csPurpose1">Purpose 1</div>
            <div id="csPurpose2">Purpose 2</div>
            <div id="csPurpose3">Purpose 3</div>
            <div id="csPurpose4">Purpose 4</div>
            <div id="csPurposes13">Purpose 1 and 3</div>
          </div>
        </div>
      </body>
    </html>
  `,
    options
  );
};
